About
========

Here is a little bit about me.


Chris Bennett
-------------
[This is my home page on Google](https://sites.google.com/site/9chrisbennett/) 

![Me](https://sites.google.com/site/9chrisbennett/_/rsrc/1359353666727/home/self.jpg?height=320&width=256)


Here is a little bit of what I like:

-   Making lots of money
-   Programming software
    - Games
    - Server Code
-   Playing with electrical circuits
-   Playing Guitar
-   Singing
-   Taking Classes
-	Astronomy




Astronomy
---------
![Astronomy](https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQ5_G1hnP5jVPnatj1w2_2dncHEGAr3L9dM1Gt0j2kiGBPdDkIZOg)
Astronomy is one of my passions.  Here are some of my favorite astronomy sites:
[Space.com](http://space.com) 




