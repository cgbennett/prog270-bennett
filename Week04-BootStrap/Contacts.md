Contacts
========

Here are a list of cool people that you should contact:


Chris Bennett
-------------
[This is my home page on Google](https://sites.google.com/site/9chrisbennett/) 
![Me](https://sites.google.com/site/9chrisbennett/_/rsrc/1359353666727/home/self.jpg?height=320&width=256)


Dalai Lama
----------
[Gotta love a selfless spiritual leader](http://www.dalailama.com/)
![Dalai Lama](http://static.guim.co.uk/sys-images/Guardian/About/General/2010/7/5/1278347413108/Dalai-Lama-006.jpg)


