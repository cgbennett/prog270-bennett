About
========

Here is a little bit about me.


Chris Bennett
-------------
[This is my home page on Google](https://sites.google.com/site/9chrisbennett/) 

![Me](https://sites.google.com/site/9chrisbennett/_/rsrc/1359353666727/home/self.jpg?height=320&width=256)


Here is a little bit of what I like:

-   Making lots of money
-   Programming software
- 	Games
- 	Server Code
-   Playing with electrical circuits
-   Playing Guitar
-   Singing
-   Taking Classes
-   Astronomy



Companies I've worked for
--------------------------


Microsoft
---------

I worked at Microsoft for a couple years.  
![Microsoft](http://cdn.cheatcc.com/news_images/Microsoft.jpg) 


Intel
---------

Intel was one of my favorite companies I worked for.  When I first moved to Seattle I had some good times there. 
![Intel](https://www.aepona.com/wp-content/uploads/2013/04/intel_logo.jpg) 


Dentrix
-------

For a brief time I worked on dental software at Dentrix in Utah.

![Dentrix](http://www.demandforce.com/_assets/images/partners/dental-dentrix_logo.jpg) 