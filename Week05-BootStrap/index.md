Astronomy
---------
Welcome to this site!  Here you can find random things that I, Chris Bennett, find interesting.  

To start off, here is a picture of the Andromedia Galaxy.  Astronomers believe it will collide with the Milky Way galaxy in about 2 billion years.  Better check your insurance coverage...

![Andromedia Galaxy, I believe](http://www.newforestobservatory.com/wordpress/wp-content/gallery/galaxies/m31_greg_noel_nfo21hours.jpg)

Astronomy is one of my passions.  Here are some of my favorite astronomy sites:

[Space.com](http://space.com) 

The ruins of ancient Rome.
![Ancient Rome](http://emcarter12.files.wordpress.com/2013/01/paul_ancient_rome.jpg)


Christmas
---------
Not be follow the crowd, but Christmas is definitely my favorite holiday.

![Beautiful Christmas Picture](http://www.womenofflowermound.org/wp-content/uploads/2013/10/christmas.jpg)

Guitar
------
I am a pretender at playing the guitar.

![Beautiful Christmas Picture](https://tunessence.com/blog/wp-content/uploads/2013/08/Electric_guitar_477101105.jpg)
